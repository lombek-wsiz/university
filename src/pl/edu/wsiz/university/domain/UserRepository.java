package pl.edu.wsiz.university.domain;

import java.util.List;

public interface UserRepository {

    void insert(User user);
    List<User> findAll();
    boolean existsByEmailAndPassword(String email, String password);
    User findByEmailAndPassword(String email, String password);
    void deleteByEmail(String email);

}
