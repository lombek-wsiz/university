package pl.edu.wsiz.university.domain;

public enum UserRole {

    STUDENT,
    TEACHER,
    ADMINISTRATOR;

    @Override
    public String toString() {
        String translatedText = "";
        switch (this) {
            case STUDENT:
                translatedText = "Student";
                break;
            case TEACHER:
                translatedText = "Nauczyciel";
                break;
            case ADMINISTRATOR:
                translatedText = "Administrator";
                break;
        }
        return translatedText;
    }

}
