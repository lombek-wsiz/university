package pl.edu.wsiz.university.domain;

import java.util.ArrayList;
import java.util.List;

public class FileUserRepository
        extends BaseFileRepository<User>
        implements UserRepository {

    private static final String FILENAME = "users.data";

    public FileUserRepository() {
        super(FILENAME);
    }

    @Override
    public boolean existsByEmailAndPassword(String email, String password) {
        List<User> users = findAll();
        for (User user: users) {
            if (user.getEmail().equalsIgnoreCase(email)
                    && user.getPassword().equals(password)
            ) {
                return true;
            }
        }
        return false;
    }

    @Override
    public User findByEmailAndPassword(String email, String password) {
        List<User> users = findAll();
        for (User user: users) {
            if (user.getEmail().equalsIgnoreCase(email)
                    && user.getPassword().equals(password)
            ) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void deleteByEmail(String email) {
        List<User> users = findAll();
        List<User> temporaryUsers = new ArrayList<>();
        for (User user: users) {
            if (!user.getEmail().equalsIgnoreCase(email)) {
                temporaryUsers.add(user);
            }
        }
        saveItems(temporaryUsers);
    }

}
