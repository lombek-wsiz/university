package pl.edu.wsiz.university.domain;

import java.io.Serializable;

public class Subject implements Serializable {

    private static final long serialVersionUID = -5189079785155215177L;

    private String name;

    public Subject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

}
