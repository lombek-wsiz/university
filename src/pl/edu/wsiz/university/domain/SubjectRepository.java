package pl.edu.wsiz.university.domain;

import java.util.List;

public interface SubjectRepository {

    void insert(Subject subject);
    List<Subject> findAll();

}
