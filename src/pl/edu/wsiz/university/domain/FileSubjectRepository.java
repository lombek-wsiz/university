package pl.edu.wsiz.university.domain;

public class FileSubjectRepository
        extends BaseFileRepository<Subject>
        implements SubjectRepository {

    private static final String FILENAME = "subjects.data";

    public FileSubjectRepository() {
        super(FILENAME);
    }

}
