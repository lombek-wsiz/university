package pl.edu.wsiz.university.domain;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFileRepository<E> {

    private final String filename;

    protected BaseFileRepository(String filename) {
        this.filename = filename;
    }

    public void insert(E item) {
        List<E> items = findAll();
        items.add(item);
        saveItems(items);
    }

    public List<E> findAll() {
        try {
            FileInputStream fileInputStream = new FileInputStream(filename);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object object = objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return (List<E>) object;
        } catch (FileNotFoundException ex) {
            return new ArrayList<>();
        } catch (IOException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void saveItems(List<E> items) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(filename);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(items);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
