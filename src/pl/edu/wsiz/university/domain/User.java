package pl.edu.wsiz.university.domain;

import java.io.Serializable;

public abstract class User implements Serializable {

    private static final long serialVersionUID = -5189079785155215177L;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private UserRole role;

    public User(String firstName, String lastName, String email,
                String password, UserRole role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserRole getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return firstName+" "+lastName;
    }

    public String getPassword() {
        return password;
    }

}
