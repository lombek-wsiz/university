package pl.edu.wsiz.university.domain;

import java.util.ArrayList;
import java.util.List;

public class ExampleUserRepository implements UserRepository {

    @Override
    public void insert(User user) {

    }

    @Override
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        list.add(new Administrator("Adam", "Kowalski", "adam@kowalski.pl",
                "Adam23"));
        list.add(new Administrator("Krzysztof", "Kowalski", "krzysztof@kowalski.pl",
                "Krzysiek12"));
        return list;
    }

    @Override
    public boolean existsByEmailAndPassword(String email, String password) {
        return false;
    }

    @Override
    public User findByEmailAndPassword(String email, String password) {
        return new Administrator("Adam", "Kowalski",
                "adam@kowalski.pl", "Adam23");
    }

    @Override
    public void deleteByEmail(String email) {

    }

}
