package pl.edu.wsiz.university.security;

import pl.edu.wsiz.university.domain.User;
import pl.edu.wsiz.university.domain.UserRepository;
import pl.edu.wsiz.university.view.BaseView;

import java.util.Scanner;

public class LoginView extends BaseView {

    private UserRepository userRepository;

    private User loggedUser;

    public LoginView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initialize() {
        String batchEnabled = System.getenv("university.login.batch.enabled");
        boolean enabled = Boolean.parseBoolean(batchEnabled);
        String email, password;
        if (enabled) {
            email = System.getenv("university.login.batch.email");
            password = System.getenv("university.login.batch.password");
            this.loggedUser = userRepository.findByEmailAndPassword(email, password);
            if (loggedUser == null) {
                System.out.println("Podane automatyczne dane są nieprawidłowe.");
                System.out.println("Nastąpi zakończenie programu");
                System.exit(0);
            }
        } else {
            System.out.println();
            super.initialize();
            System.out.println("Podaj adres email: ");
            Scanner scanner = new Scanner(System.in);
            email = scanner.nextLine();
            System.out.println("Podaj hasło: ");
            password = scanner.nextLine();
            this.loggedUser = userRepository.findByEmailAndPassword(email, password);
            if (loggedUser == null) {
                System.out.println("Podane dane są nieprawidłowe. Spróbuj ponownie");
                initialize();
            }
        }
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    @Override
    protected String getTitle() {
        return "EKRAN LOGOWANIA";
    }

}
