package pl.edu.wsiz.university.menu;

public enum AdministratorMenuItem {

    USER_LIST(1),
    ADMINISTRATOR_ADD(2),
    TEACHER_ADD(3),
    STUDENT_ADD(4),
    USER_DELETE(5),
    SUBJECT_LIST(6),
    EXIT(7);

    private int nr;

    AdministratorMenuItem(int nr) {
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }

    public String getTranslated() {
        String translatedText = "";
        switch (this) {
            case USER_LIST:
                translatedText = "lista użytkowników";
                break;
            case ADMINISTRATOR_ADD:
                translatedText = "dodaj administratora";
                break;
            case TEACHER_ADD:
                translatedText = "dodaj nauczyciela";
                break;
            case STUDENT_ADD:
                translatedText = "dodaj studenta";
                break;
            case USER_DELETE:
                translatedText = "usuń użytkownika";
                break;
            case SUBJECT_LIST:
                translatedText = "lista przedmiotów";
                break;
            case EXIT:
                translatedText = "wyjście z programu";
                break;
        }
        return translatedText;
    }

    public static AdministratorMenuItem ofNr(int nr) {
        AdministratorMenuItem[] items = values();
        for (AdministratorMenuItem item: items) {
            if (item.nr == nr) {
                return item;
            }
        }
        return EXIT;
    }

}
