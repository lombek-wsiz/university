package pl.edu.wsiz.university.menu;

import pl.edu.wsiz.university.domain.SubjectRepository;
import pl.edu.wsiz.university.domain.UserRepository;
import pl.edu.wsiz.university.view.*;

import java.util.Scanner;

public class AdministratorMenuView extends SystemMenuView {

    private final UserRepository userRepository;
    private final SubjectRepository subjectRepository;

    public AdministratorMenuView(
            UserRepository userRepository,
            SubjectRepository subjectRepository
    ) {
        this.userRepository = userRepository;
        this.subjectRepository = subjectRepository;
    }

    @Override
    public void initialize() {
        AdministratorMenuItem chosenItem;
        do {
            super.initialize();
            AdministratorMenuItem[] items = AdministratorMenuItem.values();
            for (AdministratorMenuItem item : items) {
                System.out.println(item.getNr() + " - " + item.getTranslated());
            }
            System.out.println("Twój wybór: ");
            Scanner scanner = new Scanner(System.in);
            int nr = scanner.nextInt();
            chosenItem = AdministratorMenuItem.ofNr(nr);
            switch (chosenItem) {
                case USER_LIST:
                    UserListView userListView = new UserListView(userRepository);
                    userListView.initialize();
                    break;
                case ADMINISTRATOR_ADD:
                    AdministratorDetailsView administratorDetailsView = new AdministratorDetailsView(userRepository);
                    administratorDetailsView.initialize();
                    break;
                case TEACHER_ADD:
                    TeacherDetailsView teacherDetailsView = new TeacherDetailsView(userRepository);
                    teacherDetailsView.initialize();
                    break;
                case STUDENT_ADD:
                    StudentDetailsView studentDetailsView = new StudentDetailsView(userRepository);
                    studentDetailsView.initialize();
                    break;
                case USER_DELETE:
                    UserDeleteView userDeleteView = new UserDeleteView(userRepository);
                    userDeleteView.initialize();
                    break;
                case SUBJECT_LIST:
                    SubjectListView subjectListView = new SubjectListView(subjectRepository);
                    subjectListView.initialize();
                    break;
            }
        } while (chosenItem != AdministratorMenuItem.EXIT);
    }

    @Override
    protected String getTitle() {
        return "MENU ADMINISTRATORA";
    }

}
