package pl.edu.wsiz.university.menu;

import pl.edu.wsiz.university.domain.SubjectRepository;
import pl.edu.wsiz.university.domain.UserRepository;
import pl.edu.wsiz.university.domain.UserRole;

public class SystemMenuViewFactory {

    private final UserRepository userRepository;
    private final SubjectRepository subjectRepository;

    public SystemMenuViewFactory(
            UserRepository userRepository,
            SubjectRepository subjectRepository
    ) {
        this.userRepository = userRepository;
        this.subjectRepository = subjectRepository;
    }

    public SystemMenuView create(UserRole userRole) {
        SystemMenuView systemMenuView;
        switch (userRole) {
            case ADMINISTRATOR:
                systemMenuView = new AdministratorMenuView(userRepository, subjectRepository);
                break;
            case STUDENT:
                systemMenuView = new StudentMenuView();
                break;
            case TEACHER:
                systemMenuView = new TeacherMenuView();
                break;
            default:
                throw new RuntimeException("Unsupported user role");
        }
        return systemMenuView;
    }

}
