package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.UserRepository;

import java.util.Scanner;

public class UserDeleteView extends BaseView {

    private final UserRepository userRepository;

    public UserDeleteView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void initialize() {
        super.initialize();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj adres e-mail: ");
        String email = scanner.nextLine();
        userRepository.deleteByEmail(email);
        System.out.println("Użytkownik został usunięty");
    }

    @Override
    protected String getTitle() {
        return "USUWANIE UŻYTKOWNIKA";
    }

}
