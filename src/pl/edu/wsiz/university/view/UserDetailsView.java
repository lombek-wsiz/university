package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.User;
import pl.edu.wsiz.university.domain.UserRepository;

public abstract class UserDetailsView<T extends User> extends BaseView {

    private final UserRepository userRepository;

    protected UserDetailsView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void initialize() {
        super.initialize();
        T user = createUser();
        userRepository.insert(user);
        System.out.println("Użytkownik "+user+" został dodany");
    }

    protected abstract T createUser();

}
