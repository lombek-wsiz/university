package pl.edu.wsiz.university.view;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class ListView<E> extends BaseView {

    private List<Column<E>> columns;
    private List<E> items;

    public ListView() {
        columns = new ArrayList<>();
        items = new ArrayList<>();
    }

    @Override
    public void initialize() {
        super.initialize();
        for (Column<E> column: columns) {
            System.out.print(withSpaces(column.getTitle(), column.getMaxLength()));
        }
        System.out.println();
        for (E item: items) {
            for (Column<E> column: columns) {
                System.out.print(withSpaces(
                        column.getValueProvider().apply(item),
                        column.getMaxLength()
                ));
            }
            System.out.println();
        }
    }

    protected void addColumn(String title, int maxLength, Function<E, String> valueProvider) {
        columns.add(new Column(title, maxLength, valueProvider));
    }

    protected void setItems(List<E> items) {
        this.items = items;
    }

    private String withSpaces(String text, int maxLength) {
        StringBuilder newText = new StringBuilder(text);
        int c = maxLength - text.length();
        for (int i = 1; i <= c; i++) {
            newText.append(" ");
        }
        return newText.toString();
    }

}
