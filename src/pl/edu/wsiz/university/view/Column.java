package pl.edu.wsiz.university.view;

import java.util.function.Function;

public class Column<E> {

    private String title;
    private int maxLength;
    private Function<E, String> valueProvider;

    public Column(String title, int maxLength, Function<E, String> valueProvider) {
        this.title = title;
        this.maxLength = maxLength;
        this.valueProvider = valueProvider;
    }

    public String getTitle() {
        return title;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public Function<E, String> getValueProvider() {
        return valueProvider;
    }
}
