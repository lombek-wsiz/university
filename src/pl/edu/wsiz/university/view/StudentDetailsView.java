package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.Student;
import pl.edu.wsiz.university.domain.UserRepository;

import java.util.Scanner;

public class StudentDetailsView extends UserDetailsView<Student> {

    public StudentDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "DODAWANIE STUDENTA";
    }

    @Override
    protected Student createUser() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        System.out.println("Podaj e-mail: ");
        String email = scanner.nextLine();

        System.out.println("Podaj hasło: ");
        String password = scanner.nextLine();

        System.out.println("Podaj nr albumu: ");
        long albumNumber = scanner.nextLong();

        return new Student(firstName, lastName, email, password, albumNumber);
    }

}
