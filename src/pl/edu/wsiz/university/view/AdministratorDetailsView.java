package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.Administrator;
import pl.edu.wsiz.university.domain.UserRepository;

import java.util.Scanner;

public class AdministratorDetailsView extends UserDetailsView<Administrator> {

    public AdministratorDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "DODAWANIE ADMINISTRATORA";
    }

    @Override
    protected Administrator createUser() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        System.out.println("Podaj e-mail: ");
        String email = scanner.nextLine();

        System.out.println("Podaj hasło: ");
        String password = scanner.nextLine();

        return new Administrator(firstName, lastName, email, password);
    }

}
