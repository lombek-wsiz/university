package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.Subject;
import pl.edu.wsiz.university.domain.SubjectRepository;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class SubjectListView extends ListView<Subject> {

    private static final int SUBJECT_NAME_LENGTH = 25;

    private final SubjectRepository subjectRepository;

    public SubjectListView(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public void initialize() {
        addColumn("Nazwa", SUBJECT_NAME_LENGTH, new Function<Subject, String>() {
            @Override
            public String apply(Subject subject) {
                return subject.getName();
            }
        });
        List<Subject> subjects = subjectRepository.findAll();
        subjects.sort(new Comparator<Subject>() {
            @Override
            public int compare(Subject o1, Subject o2) {
                return o1.getName()
                        .compareToIgnoreCase(o2.getName());
            }
        });
        setItems(subjects);
        super.initialize();
    }

    @Override
    protected String getTitle() {
        return "LISTA PRZEDMIOTÓW";
    }

}
