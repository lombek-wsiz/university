package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.User;
import pl.edu.wsiz.university.domain.UserRepository;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class UserListView extends ListView<User> {

    private static final int USER_FIRST_NAME_MAX_LENGTH = 12;
    private static final int USER_LAST_NAME_MAX_LENGTH = 15;
    private static final int USER_EMAIL_MAX_LENGTH = 20;
    private static final int USER_ROLE_MAX_LENGTH = 10;

    private final UserRepository userRepository;

    public UserListView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initialize() {
        addColumn("Imię", USER_FIRST_NAME_MAX_LENGTH, new Function<User, String>() {
            @Override
            public String apply(User user) {
                return user.getFirstName();
            }
        });
        addColumn("Nazwisko", USER_LAST_NAME_MAX_LENGTH, new Function<User, String>() {
            @Override
            public String apply(User user) {
                return user.getLastName();
            }
        });
        addColumn("E-mail", USER_EMAIL_MAX_LENGTH, new Function<User, String>() {
            @Override
            public String apply(User user) {
                return user.getEmail();
            }
        });
        addColumn("Funkcja", USER_ROLE_MAX_LENGTH, new Function<User, String>() {
            @Override
            public String apply(User user) {
                return user.getRole().toString();
            }
        });
        List<User> users = userRepository.findAll();
        users.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                int c = o1.getLastName()
                        .compareToIgnoreCase(o2.getLastName());
                if (c == 0) {
                    return o1.getFirstName()
                            .compareToIgnoreCase(o2.getFirstName());
                }
                return c;
            }
        });
        setItems(users);
        super.initialize();
    }

    @Override
    protected String getTitle() {
        return "LISTA UŻYTKOWNIKÓW";
    }

}
