package pl.edu.wsiz.university.view;

import pl.edu.wsiz.university.domain.Teacher;
import pl.edu.wsiz.university.domain.UserRepository;

import java.util.Scanner;

public class TeacherDetailsView extends UserDetailsView<Teacher> {

    public TeacherDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "DODAWANIE NAUCZYCIELA";
    }

    @Override
    protected Teacher createUser() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        System.out.println("Podaj e-mail: ");
        String email = scanner.nextLine();

        System.out.println("Podaj hasło: ");
        String password = scanner.nextLine();

        System.out.println("Podaj tytuł naukowy: ");
        String academicDegree = scanner.nextLine();

        return new Teacher(firstName, lastName, email, password, academicDegree);
    }

}
