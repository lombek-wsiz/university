package pl.edu.wsiz.university;

import pl.edu.wsiz.university.domain.*;
import pl.edu.wsiz.university.menu.SystemMenuView;
import pl.edu.wsiz.university.menu.SystemMenuViewFactory;
import pl.edu.wsiz.university.security.LoginView;

import java.util.function.Consumer;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {
        Student student1 = new Student("Adam", "Kowalski",
                "adam@kowalski.pl", "Adam1", 3445L);
        Student student2 = new Student("Jan", "Nowak",
                "jann@gmail.com", "janek", 53445L);

        Teacher teacher1 = new Teacher("Witold", "Kubica",
                "kubwi@gmail.com", "qwert6", "mgr");

        Administrator administrator1 = new Administrator("Mariusz", "Małysz",
                "m@mal.pl", "234");
        Administrator administrator2 = new Administrator("Krzysztof", "Nowak",
                "kkrzysz@gmail.com", "o0351fF");
        Administrator administrator3 = new Administrator("Filip", "Nowak",
                "ff@gmail.com", "o0351fF");

        Subject subject1 = new Subject("Języki i metody programowania");
        Subject subject2 = new Subject("Bazy danych");
        Subject subject3 = new Subject("Programowanie w języku C++");

//        UserRepository userRepository = new ExampleUserRepository();
        UserRepository userRepository = new FileUserRepository();
        SubjectRepository subjectRepository = new FileSubjectRepository();

        LoginView loginView = new LoginView(userRepository);
        loginView.initialize();

        System.out.println();
        System.out.println("=========== ZALOGOWANY UŻYTKOWNIK ===========");
        User loggedUser = loginView.getLoggedUser();
        System.out.println(loggedUser.getFirstName()+" "+loggedUser.getLastName()+" ("+loggedUser.getEmail()+")");
        System.out.println("Funkcja: "+loggedUser.getRole());
        System.out.println();

        SystemMenuViewFactory systemMenuViewFactory = new SystemMenuViewFactory(userRepository, subjectRepository);
        SystemMenuView systemMenuView = systemMenuViewFactory.create(loggedUser.getRole());
        systemMenuView.initialize();
    }

    private static void exampleFunction() {
        Function<String, Integer> f1 = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        };
        System.out.println(f1.apply("napis"));
        System.out.println(f1.apply("znak3585"));
    }

    private static void exampleConsumer() {
        Consumer<Integer> consumer = new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer * 2);
            }
        };
        consumer.accept(-10);
        consumer.accept(500);
        consumer.accept(4551);
    }

    private static void examplePair() {
        Pair<Integer, String> p1 = new Pair<>(5, "text123");
        System.out.println(p1.getLeft());
        System.out.println(p1.getRight());

        Pair<String, Double> p2 = new Pair<>("111", 5.56);
        System.out.println(p2.getLeft());
        System.out.println(p2.getRight());
    }

    private static void findByEmailExamples(UserRepository userRepository) {
        User user1 = userRepository.findByEmailAndPassword("kubwi@gmail.com", "qwert6");
        User user2 = userRepository.findByEmailAndPassword("kubwi@gmail.com", "xx");
    }

    private static void userRoles() {
        UserRole userRole1 = UserRole.ADMINISTRATOR;
        UserRole userRole2 = UserRole.STUDENT;
        UserRole[] values = UserRole.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }
    }

    private static void result(UserRepository userRepository) {
        boolean result1 = userRepository
                .existsByEmailAndPassword("adam@kowalski.pl", "Adam1");
        System.out.println("result1 = "+result1);

        boolean result2 = userRepository
                .existsByEmailAndPassword("adam@kowalski.pl", "Adam14");
        System.out.println("result2 = "+result2);

        boolean result3 = userRepository
                .existsByEmailAndPassword("adam@kowalski.pl5", "Adam1");
        System.out.println("result3 = "+result3);

        boolean result4 = userRepository
                .existsByEmailAndPassword("ADAM@KOWALSKI.PL", "Adam1");
        System.out.println("result4 = "+result4);
    }

    private static void insert(Student student1, Student student2, Teacher teacher1, UserRepository userRepository) {
        userRepository.insert(teacher1);
        userRepository.insert(student1);
        userRepository.insert(student2);
        System.out.println("Dodano");
    }

}
